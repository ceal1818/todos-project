package com.example.todos.repositories;

import java.util.ArrayList;

public interface EntityRepository<M, I> {

	public M get(I id);
	
	public ArrayList<M> get();
	
	public void add(M entity);
	
	public void set(I id, M entity);
	
	public void remove(M entity);
	
}
