package com.example.todos.services;

public interface RetrieveByParamService<R, P> {

	public R execute(P param);
	
}
