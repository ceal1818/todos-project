package com.example.todos.services;

public interface RetrieveService<R> {

	public R execute();
}
