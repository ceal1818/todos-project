package com.example.todos.resources;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.todos.entities.Todo;

@RestController
@RequestMapping(path="/todos")
public class TodoResources {

	@RequestMapping(method=RequestMethod.GET, 
			headers={"content-type=application/json"})
	public List<Todo> getTodos(){
		return null;
	}

	@RequestMapping(method=RequestMethod.GET, path="/{id}", 
			headers={"content-type=application/json"})	
	public ResponseEntity<Todo> getTodoById(
			@PathVariable(name="id", required=true) Integer id) {
		return null;
	}

	@RequestMapping(method=RequestMethod.POST, 
			headers={"content-type=application/json"})
	public ResponseEntity<Todo> createTodo(@RequestBody Todo todo) {
		return null;
	}
	
	@RequestMapping(method=RequestMethod.PUT, path="/{id}",
			headers={"content-type=application/json"})
	public ResponseEntity<Todo> updateTodo(@PathVariable(name="id", required=true) Integer id, @RequestBody Todo todo) {
		return null;
	}
	
	@RequestMapping(method=RequestMethod.DELETE, path="/{id}",
			headers={"content-type=application/json"})
	public ResponseEntity<Todo> removeTodo(@PathVariable(name="id", required=true) Integer id) {
		return null;
	}		
}
