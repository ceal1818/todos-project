package com.example.todos.entities;

import java.util.Date;

public class Todo {

	private int id;
	
	private String title;
	
	private String content;
	
	private String[] tags;
	
	private Date createdDate;
	
	private Date modifiedDate;
	
	public Todo() {
		// TODO Auto-generated constructor stub
	}

	public Todo(String title, String content, String[] tags, Date createdDate, Date modifiedDate) {
		super();
		this.title = title;
		this.content = content;
		this.tags = tags;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String[] getTags() {
		return tags;
	}

	public void setTags(String[] tags) {
		this.tags = tags;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public void addTag(String tag) {
		
	}

	public String getTag(int index) {
		return null;
	}	
	
}
